terraform {
  cloud {
    hostname = "app.terraform.io"
    organization = "by0000"

    workspaces {
      name = "tf-class"
    }
  }
}
