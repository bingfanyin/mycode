# this is a test for simple-go-service
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.22.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "simplegoservice" {
  name         = "registry.gitlab.com/alta3/simplegoservice:latest"
  keep_locally = true    // keep image after "destroy"
}

resource "docker_container" "goservice" {
  image = docker_image.simplegoservice.image_id
  name  = "go-service"
  ports {
    internal = 9876
    external = 5432
  }
}

